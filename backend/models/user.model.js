const mongoose = require("mongoose");

//place to write your schima
const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },

    email: {
      type: String,
      unique: true,
      required: true,
    },

    password: {
      type: String,
      required: true,
    },

    gender: {
      type: String,
      enum: ["Male", "Female", "Other"],
    },

    nationality: {
      type: String,
      default: "Nepali",
    },

    address: {
      type: String,
      // required: true,
    },

    date_of_birth: {
      type: Date,
    },

    role: {
      type: String,
      enum: ["Buyer", "Seller"],
      default: "Buyer",
    },

    image: {
      type: String,
    },

    is_admin: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const User = mongoose.model("User", UserSchema);

module.exports = User;
