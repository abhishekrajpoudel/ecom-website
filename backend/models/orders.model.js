const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema({
  buyer_id: {
    type: mongoose.Types.ObjectId,
    ref: "User",
    default: null,
  },
  product_id: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Product",
      default: null,
    },
  ],
  total_price: Number,
  final_price: Number,
});
