const mongoose = require("mongoose");
const ProductSchima = new mongoose.Schema({
  //catagoryID
  product_name: {
    type: String,
    required: true,
  },
  catagory_id: {
    type: mongoose.Types.ObjectId,
    ref: "Category",
    // required: true,
  },
  seller_id: { type: mongoose.Types.ObjectId, ref: "User", default: null },
  product_price: {
    type: Number,
    required: true,
  },
  product_discount: {
    type: Number,
  },
  //   product Image here
  product_discription: {
    type: String,
  },
  image: [String],
});

const Product = mongoose.model("Product", ProductSchima);

module.exports = Product;
