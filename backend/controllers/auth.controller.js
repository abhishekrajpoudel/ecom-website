const UserModel = require("../models/user.model");
const passwordHash = require("password-hash");
const jwt = require("jsonwebtoken");
const jwt_secret = require("../configs/config");

/**
 * jwt.sign function
 * Takes two parameter
 *    1st = payload => the data you are trying to store in your token
 *        eg users id, name, email, user's role.
 * 2nd = secret/ privatekey
 *    Any type of private value. You can write anything
 *    This private key helps make the token. All the token generated
 *    are according to your private key you have here.
 *    This private key is also used to decript the token and get the
 *    data in payload
 */
//changes yo class vanda baira rakhyo
function tokenGenerate(user) {
  const token = jwt.sign(
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      is_admin: user.is_admin,
    },
    jwt_secret
  );
  return token;
}

class AuthController {
  login(req, res, next) {
    UserModel.findOne({ email: req.body.email })
      .then((user) => {
        //this passwordHash uses 2 param
        //1 plain text 2 hash value
        // so the plain text comes from our user logging in and hash value
        // comes form the server
        if (user) {
          //yo eeuta use add garyo
          if (!passwordHash.verify(req.body.password, user.password)) {
            res.status(200).json({
              data: null,
              msg: "Password password not match",
              status: 401, //This is unauthorized status
              // to know what all the status number mean
              //https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
            });
          }

          // generate token
          // for this we use Jwt -> Json Web Token
          // https://jwt.io/

          res.json({
            //using res.json because it automatically give status 200
            data: { user: user, token: tokenGenerate(user) },
            msg: "User Found ",
            status: 200,
          });
        } else {
          //yo eeuta else condition add garyo
          res.status(200).json({
            data: null,
            msg: "User not found",
            status: 404,
          });
        }
      })
      .catch((error) => {
        console.log("err =>", error);
        res.status(200).json({
          data: null,
          msg: error,
          status: 404,
        });
        next(error);
      }); //this is an async function
  }
}

module.exports = AuthController;
