let successOutput, errorOutput;
//Internal Output giving functions
function successResponse(response, success) {
  response.json({ data: success, status: 200, msg: successOutput });
}
function errorResponse(response, error) {
  response.json({ data: error, status: 400, msg: errorOutput });
}

//functions that are exported
function getAllData(response, Model, outputName) {
  successOutput = `All ${outputName} found`;
  errorOutput = ` Error finding ${outputName}`;
  Model.find()
    .then((success) => {
      successResponse(response, success);
    })
    .catch((error) => {
      errorResponse(response, error);
    });
}

function addData(response, Model, outputName) {
  successOutput = `Success Adding ${outputName}`;
  errorOutput = ` Error adding ${outputName}`;
  console.log("Model = ", Model);
  Model.save()
    .then((success) => {
      successResponse(response, success);
    })
    .catch((error) => {
      errorResponse(response, error);
    });
}

function getDataById(request, response, Model, outputName) {
  successOutput = `${outputName} found successfully`;
  errorOutput = ` Error finding ${outputName}`;
  Model.findOne({
    _id: request.params.id,
  })
    .then((success) => {
      successResponse(response, success);
    })
    .catch((error) => {
      errorResponse(response, error);
    });
}

function updateDataById(request, response, Model, data, outputName) {
  successOutput = `${outputName} updated successfully`;
  errorOutput = ` Error updating ${outputName}`;
  Model.updateOne({ _id: request.params.id }, { $set: data }, { upsert: true })
    .then((success) => {
      successResponse(response, success);
    })
    .catch((error) => {
      errorResponse(response, error);
    });
}

function deleteDataById(request, response, Model, outputName) {
  successOutput = `${outputName} deleted successfully`;
  errorOutput = ` Error deleting ${outputName}`;
  Model.deleteOne({
    _id: request.params.id,
  })
    .then((success) => {
      successResponse(response, success);
    })
    .catch((error) => {
      errorResponse(response, error);
    });
}

function uploadImage(request, data) {
  if (request.file) {
    data.image = request.file.filename;
  }
  return data;
}

module.exports = {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
  uploadImage,
};
