const UserModel = require("../models/user.model");
const passwordHash = require("password-hash");
const fs = require("fs");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
  uploadImage,
} = require("./base.controller");

class UserController {
  registerUser(req, res, next) {
    let user = new UserModel(req.body);
    user = uploadImage(req, user);
    user.password = passwordHash.generate(req.body.password);
    addData(res, user, "User");
  }

  getAllUsers(req, res, next) {
    getAllData(res, UserModel, "Users");
  }

  updateUserById(req, res, next) {
    let userData = req.body;
    userData = uploadImage(req, userData);
    updateDataById(req, res, UserModel, userData, "User");
  }

  getUserById(req, res, next) {
    getDataById(req, res, UserModel, "User");
  }

  deleteUserById(req, res, next) {
    deleteDataById(req, res, UserModel, "User");
  }
}

module.exports = UserController;
