const mongoose = require("mongoose");
const dbUrl = "mongodb://localhost:27017/stack-1";

mongoose.connect(dbUrl, (err, success) => {
  if (err) {
    console.log(`Error >>>> ${err}`);
  } else {
    console.log("Connected to Mongoose");
  }
});
