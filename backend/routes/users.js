const express = require("express");
const router = express.Router();
const controller = require("../controllers/user.controller");
//File Uploading
const upload = require("../middleware/uploader.js");
const userController = new controller();
/* GET users listing. */
router
  .route("/")
  .post(upload.single("image"), userController.registerUser)
  .get(userController.getAllUsers);

router
  .route("/:id")
  .get(userController.getUserById)
  .put(upload.single("image"), userController.updateUserById)
  .delete(userController.deleteUserById);

module.exports = router;
