const router = require("express").Router();
const CategoryModel = require("../models/catagory.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
} = require("../controllers/base.controller");

let category = "Category";
let categories = "Categories";

router
  .route("/")
  .get((req, res, next) => {
    getAllData(res, CategoryModel, categories);
  })
  .post((req, res, next) => {
    console.log("Category Model ", CategoryModel);
    let categoryModel = new CategoryModel(req.body);

    addData(res, categoryModel, category);
  });

router
  .route("/:id")
  .get((req, res, next) => {
    getDataById(req, res, CategoryModel, category);
  })
  .put((req, res, next) => {
    let data = req.body;
    updateDataById(req, res, CategoryModel, data, category);
  })
  .delete((req, res, next) => {
    deleteDataById(req, res, CategoryModel, category);
  });

module.exports = router;
