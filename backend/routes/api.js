const userRoute = require("./users");
const authRoute = require("./auth");
const adminRoute = require("./admin");
const categoryRoute = require("./category");
const productRoute = require("./product");
const isLoggedIn = require("../middleware/isLoggedIn");
const isAdmin = require("../middleware/isAdmin");
const router = require("express").Router();

// these are all middlewares
//spicifically these are routing level middleware

router.use("/auth", authRoute);

//in this case this route has to go through the isLoggedIn middleware
// to go to adminRoute function

//Note => isAdmin should be called after isLoggedIn
//because the setting user is done by isLoggedIn and if you call isAdmin first
// then it wont work
// to call multiple middleware in one function . Make them into a collection using []
router.use("/admin", [isLoggedIn, isAdmin], adminRoute);
router.use("/category", [isLoggedIn, isAdmin], categoryRoute);
router.use("/users", userRoute);
router.use("/product", productRoute);
module.exports = router;
