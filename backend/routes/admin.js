var express = require("express");
var router = express.Router();
const adminController = require("../controllers/admin.controller");

const admin = new adminController();

router.route("/").get(admin.dashboard);

module.exports = router;
