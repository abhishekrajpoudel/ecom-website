const router = require("express").Router();
const ProductModel = require("../models/product.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
} = require("../controllers/base.controller");

const uploader = require("../middleware/uploader");
const product = "Product";
router
  .route("/")
  .get((req, res, next) => {
    getAllData(res, ProductModel, "Products");
  })
  .post((req, res, next) => {
    addData(res, ProductModel, product);
  });

router
  .route("/:id")
  .get((req, res, next) => {
    getDataById(req, res, ProductModel, product);
  })
  .put((req, res, next) => {
    updateDataById(req, res, ProductModel, req.body, product);
  })
  .delete((req, res, next) => {
    deleteDataById(req, res, ProductModel, product);
  });

module.exports = router;
