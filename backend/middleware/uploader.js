const multer = require("multer");

const myStorage = multer.diskStorage({
  filename: function (req, file, cb) {
    const file_name = Date.now() + file.originalname;
    cb(null, file_name);
  },
  destination: function (req, file, cb) {
    cb(null, process.cwd() + "/public/images/");
  },
});

const upload = multer({
  storage: myStorage,
});

module.exports = upload;
