const jwt = require("jsonwebtoken");
const jwt_secret = require("../configs/config");
/**
 *
 * because this is the middleware until you pass
 * the value other middleware will not be called
 */
const isLoggedIn = (req, res, next) => {
  let token = null;
  if (req.headers["authorization"]) {
    token = req.headers["authorization"];
  }
  //sometime the header might be called x-access-token
  if (req.headers["x-access-token"]) {
    token = req.headers["x-access-token"];
  }
  //sometime the token might come in a query then
  if (req.query.token) {
    token = req.query.token;
  }

  if (token === null) {
    next("unauthorized access");
  }

  const data = jwt.verify(token, jwt_secret);

  if (!data._id) {
    next("unauthorized access.");
  }

  req.user = data;

  next();
};

module.exports = isLoggedIn;
