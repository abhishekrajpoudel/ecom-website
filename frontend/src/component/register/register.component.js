import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { success, error } from "../../Utils/utils";
import { HttpClient } from "../../Utils/httpClients";
import { Header } from "../header/headerComponent";

export function Register() {
  const commonFields = {
    name: "",
    email: "",
    password: "",
    re_password: "",
    gender: "",
  };

  const [formValues, setFormValues] = useState(commonFields);
  const [formErrors, setFormErrors] = useState(commonFields);
  const [isSubmit, setIsSubmit] = useState(false);

  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      const http = new HttpClient();

      http
        .postItem("users", formValues, {
          "content-type": "application/json",
        })
        .then((response) => {
          if (response.data.status === 200) {
            success(response.data.msg);
            setIsSubmit(false);

            localStorage.setItem("register_success", true);
            navigate("/login");
          } else {
            error(response.data.msg);
          }
          // console.log('response: ', response)
        })
        .catch((error) => {
          console.log("Error: ", error);
        });
    }
  });

  const validate = (values) => {
    const errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i; //find out about regex
    if (!values.name) {
      errors.name = "Username is required!";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not a valid email format!";
    }
    if (!values.password) {
      errors.password = "Password is required";
    } else if (values.password.length < 4) {
      errors.password = "Password must be more than 4 characters";
    }
    if (!values.re_password) {
      errors.re_password = "re-password is required";
    } else if (!values.re_password === values.password) {
      errors.re_password = "Password didn't match";
    }
    if (!values.gender) {
      errors.gender = "Please select gender";
    }
    return errors;
  };

  return (
    <>
      <Header></Header>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="text-center">Register Page</h4>
            <hr />
            <div className="row mb-3">
              <form action="" onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label className="form-label text-center">Name</label>
                  <input
                    onChange={handleChange}
                    name="name"
                    type="text"
                    className="form-control"
                    placeholder="Enter your name"
                    required
                  />
                  <span className="text-danger">{formErrors.name}</span>
                </div>

                <div className="mb-3">
                  <label className="form-label text-center">
                    Email address
                  </label>
                  <input
                    onChange={handleChange}
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Enter your email"
                  />
                  <span className="text-danger">{formErrors.email}</span>
                  <div id="emailHelp" className="form-text">
                    We'll never share your email with anyone else.
                  </div>
                </div>

                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input
                    onChange={handleChange}
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Enter your password"
                  />
                  <span className="text-danger">{formErrors.password}</span>
                </div>

                <div className="mb-3">
                  <label className="form-label">Re-Password</label>
                  <input
                    onChange={handleChange}
                    name="re_password"
                    type="password"
                    className="form-control"
                    placeholder="Re-Enter your password"
                    required
                  />
                  <span className="text-danger">{formErrors.re_password}</span>
                </div>
                <div className="row mb-3">
                  <label className="col-sm-1">Gender</label>
                  <div className="col-sm-1">
                    <label htmlFor="male">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Male"}
                        id="male"
                        className="ml-3"
                      />{" "}
                      Male
                    </label>
                  </div>
                  <div className="col-sm-1">
                    <label htmlFor="female">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Female"}
                        id="female"
                        className="ml-3"
                      />{" "}
                      Female
                    </label>
                  </div>

                  <div className="col-sm-1">
                    <label htmlFor="Other">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Other"}
                        id="Other"
                        className="ml-3"
                      />{" "}
                      Other
                    </label>
                  </div>
                  <span className="text-danger">{formErrors.gender}</span>
                </div>

                <button
                  onClick={handleSubmit}
                  type="submit"
                  className="btn btn-success"
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
