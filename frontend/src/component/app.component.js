import AppRouting from "./appRouting";
import { ToastContainer } from "react-toastify";

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <AppRouting></AppRouting>
      <ToastContainer></ToastContainer>
    </div>
  );
}

export default App;
