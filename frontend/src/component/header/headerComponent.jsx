import React from "react";
import { FaUserAlt } from "react-icons/fa";
import { NavLink } from "react-router-dom";

function CommonNav() {
  return (
    <>
      <li className="nav-link">
        <NavLink to="/">Home</NavLink>
      </li>
    </>
  );
}

export function Header() {
  return (
    <header>
      <ul className="nav">
        <CommonNav></CommonNav>
        <li className="nav-link">
          <NavLink to="/login">Login</NavLink>
        </li>
        <li className="nav-link">
          <NavLink to="/register">Register</NavLink>
        </li>
      </ul>
    </header>
  );
}

export function AdminHeader() {
  return (
    <header>
      <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        {/* <!-- Topbar Navbar --> */}
        <ul className="navbar-nav ml-auto">
          <div className="topbar-divider d-none d-sm-block"></div>

          {/* <!-- Nav Item - User Information --> */}
          <li className="nav-item dropdown no-arrow">
            <NavLink
              className="nav-link dropdown-toggle"
              to="/"
              id="userDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <span className="mr-2 d-none d-lg-inline text-gray-600 small">
                Abhishek Raj Poudel
              </span>
              <FaUserAlt></FaUserAlt>
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
