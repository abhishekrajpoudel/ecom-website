import React, { useEffect } from "react";

import { HomeComponent } from "./HomeComponent";
import {
  BrowserRouter,
  Route,
  Routes,
  Navigate,
  useNavigate,
} from "react-router-dom";
import { Login, Logout } from "./login/login.component";
import { Register } from "./register/register.component";

import * as Admin from "./Admin/index";

const token = "token";

function PrivateRoute({ children }) {
  const isLoggedIn = localStorage.getItem(token);
  return isLoggedIn ? children : <Navigate to="/admin" />;
}

function Success() {
  let is_success = localStorage.getItem("register_success");
  let navigate = useNavigate();

  useEffect(() => {
    setTimeout(() => {
      navigate("/login");
    }, 3000);
  });
  if (is_success) {
    return <>You have registered successfully.</>;
  } else {
    return <Navigate to="/login"></Navigate>;
  }
}

function AppRouting() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomeComponent />} />

        <Route path="/success" element={<Success />} />

        <Route path="/login" element={<Login />} />

        <Route path="/register" element={<Register />} />

        <Route
          path="/admin"
          element={
            <PrivateRoute>
              <Admin.AdminLayout></Admin.AdminLayout>
            </PrivateRoute>
          }
        >
          <Route index element={<Admin.Dashboard></Admin.Dashboard>} />

          <Route path="category" element={<Admin.Category></Admin.Category>} />
          <Route
            path="category/create"
            element={
              <PrivateRoute>
                <Admin.CategoryCreate></Admin.CategoryCreate>
              </PrivateRoute>
            }
          />
          <Route
            path="category/:id"
            element={
              <PrivateRoute>
                <Admin.CategoryEdit></Admin.CategoryEdit>
              </PrivateRoute>
            }
          />

          <Route path="users" element={<Admin.Users></Admin.Users>} />
          <Route
            path="users/create"
            element={
              <PrivateRoute>
                <Admin.UserCreate></Admin.UserCreate>
              </PrivateRoute>
            }
          />

          <Route
            path="users/:id"
            element={
              <PrivateRoute>
                <Admin.UserEdit></Admin.UserEdit>
              </PrivateRoute>
            }
          />

          <Route path="products" element={<Admin.Products></Admin.Products>} />
          <Route
            path="products/create"
            element={
              <PrivateRoute>
                <Admin.ProductCreate></Admin.ProductCreate>
              </PrivateRoute>
            }
          />

          <Route
            path="products/:id"
            element={
              <PrivateRoute>
                <Admin.ProductEdit></Admin.ProductEdit>
              </PrivateRoute>
            }
          />
        </Route>

        <Route path="/logout" element={<Logout />} />
      </Routes>
    </BrowserRouter>
  );
}

export default AppRouting;
