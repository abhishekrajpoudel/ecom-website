import React from "react";
import { Header } from "./header/headerComponent";

export function HomeComponent() {
  return (
    <div>
      <Header></Header>
      <h1>Welcome to home</h1>
    </div>
  );
}
