import { HttpClient } from "../../../Utils/httpClients";

export default function DeleteItemByID({ id, route }) {
  const http = new HttpClient();
  http
    .deleteItem(`${route}/${id}`, true)
    .then((response) => {
      if (response.data.status === 200) {
        success(response.data.msg);
        setIsLoading(true);
      } else {
        error(response.data.msg);
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
