import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { HttpClient } from "../../../Utils/httpClients";
import { success } from "../../../Utils/utils";
import UserForm from "./UserForm";

export default function UserEdit() {
  const http = new HttpClient();
  const navigate = useNavigate();
  const [data, setData] = useState({});

  const [is_loading, setIsLoading] = useState(true);

  const param = useParams();

  useEffect(() => {
    http
      .getItemById("users/" + param.id, true)
      .then((response) => {
        if (response.data.status === 200) {
          setData(response.data.data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.error("Error", error);
      });
  }, [is_loading]);

  const edit = (data) => {
    http
      .updateItem("users/" + param.id, data, true)
      .then((response) => {
        if (response.data.status === 200) {
          success(response.data.msg);
          navigate("/admin/users");
        }
      })
      .catch((error) => {
        console.error("Error", error);
      });
  };

  return <UserForm onHandleSubmit={edit} data={data}></UserForm>;
}
