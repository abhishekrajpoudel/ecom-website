import React from "react";
import { useNavigate } from "react-router-dom";
import { HttpClient } from "../../../Utils/httpClients";
import { success } from "../../../Utils/utils";
import UserForm from "./UserForm";

export default function UserCreate() {
  const http = new HttpClient();
  const navigate = useNavigate();

  const add = (data) => {
    http
      .postItem("users", data, {
        "content-type": "application/json",
        headers: {
          authorization: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        if (response.data.data) {
          success(response.data.data);
          navigate("/admin/users");
        }
      })
      .catch((error) => {
        console.log("error ", error);
      });
  };

  return <UserForm onHandleSubmit={add} data={null}></UserForm>;
}
