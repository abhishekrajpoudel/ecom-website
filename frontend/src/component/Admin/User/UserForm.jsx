import React, { useEffect, useState } from "react";

export default function UserForm({ data, onHandleSubmit }) {
  const commonFields = {
    name: "",
    email: "",
    gender: "",
    is_admin: false,
  };

  const [formValues, setFormValues] = useState(commonFields);
  const { name, email, is_admin } = formValues;
  const [formErrors, setFormErrors] = useState(commonFields);
  //   const [isSubmit, setIsSubmit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  useEffect(() => {
    if (data) {
      const { name, email, password, re_password, gender, is_admin } = data;
      setFormValues({
        name: name,
        email: email,
        password: password,
        re_password: re_password,
        gender: gender,
        is_admin: is_admin,
      });
    } else {
      setIsLoading(true);
    }
    console.log(data);
  }, [isLoading, data]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setFormErrors(validate(formValues));
    onHandleSubmit({
      name: formValues.name,
      email: formValues.email,
      password: formValues.password,
      re_password: formValues.re_password,
      gender: formValues.gender,
      is_admin: formValues.is_admin,
    });
  };

  const validate = (values) => {
    const errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i; //find out about regex
    if (!values.name) {
      errors.name = "Username is required!";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not a valid email format!";
    }

    if (!values.gender) {
      errors.gender = "Please select gender";
    }
    return errors;
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="text-center">Register Page</h4>
            <hr />
            <div className="row mb-3">
              <form action="" onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label className="form-label text-center">Name</label>
                  <input
                    onChange={handleChange}
                    name="name"
                    type="text"
                    className="form-control"
                    value={name ? name : ""}
                    placeholder="Enter your name"
                    required
                  />
                  <span className="text-danger">{formErrors.name}</span>
                </div>

                <div className="mb-3">
                  <label className="form-label text-center">
                    Email address
                  </label>
                  <input
                    onChange={handleChange}
                    name="email"
                    type="email"
                    value={email ? email : ""}
                    className="form-control"
                    placeholder="Enter your email"
                  />
                  <span className="text-danger">{formErrors.email}</span>
                </div>

                <div className="mb-3">
                  <label className="form-label text-center">Is Admin</label>{" "}
                  <input
                    onChange={handleChange}
                    name="is_admin"
                    type="checkbox"
                    checked={is_admin ? true : false}
                  />
                </div>

                <div className="row mb-3">
                  <label className="col-sm-1">Gender</label>
                  <div className="col-sm-1">
                    <label htmlFor="male">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Male"}
                        id="male"
                        className="ml-3"
                      />{" "}
                      Male
                    </label>
                  </div>
                  <div className="col-sm-1">
                    <label htmlFor="female">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Female"}
                        id="female"
                        className="ml-3"
                      />{" "}
                      Female
                    </label>
                  </div>

                  <div className="col-sm-1">
                    <label htmlFor="Other">
                      <input
                        onChange={handleChange}
                        name="gender"
                        type="radio"
                        value={"Other"}
                        id="Other"
                        className="ml-3"
                      />{" "}
                      Other
                    </label>
                  </div>
                  <span className="text-danger">{formErrors.gender}</span>
                </div>

                <button
                  onClick={handleSubmit}
                  type="submit"
                  className="btn btn-success"
                >
                  {data ? "Update" : "Create"} User
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
