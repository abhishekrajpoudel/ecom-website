import React, { useState, useEffect } from "react";
import { error, success } from "../../../Utils/utils";
import { HttpClient } from "../../../Utils/httpClients";
import { NavLink } from "react-router-dom";
import { FaPlus } from "react-icons/fa";
import DataTable from "react-data-table-component";
import GetActions from "../Action-Button/actionBtnsComponent";

export default function Users() {
  let [allUsers, setAllUsers] = useState([]);
  const [is_loading, setIsLoading] = useState(true);
  const http = new HttpClient();

  const columns = [
    { name: "Name", selector: (row) => row.name, sortable: true },
    { name: "Email", selector: (row) => row.email, sortable: true },
    { name: "Gender", selector: (row) => row.gender },
    { name: "Nationality", selector: (row) => row.nationality },
    { name: "Role", selector: (row) => row.role },
    { name: "Is Admin", selector: (row) => row.is_admin },
    { name: "Actions", selector: (row) => row.actions },

    // <th>Is Admin</th>
    // <th>Action</th>
  ];

  useEffect(() => {
    http
      .getItem(`users`, {
        header: {
          authorization: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        let responseValue = response.data.data;
        if (!response.data.data) {
          error("No user found 😥");
        }
        let allvalues = responseValue.map((object, index) => {
          object.is_admin = object.is_admin ? "Yes" : "No";
          object.actions = (
            <GetActions
              onDelete={deleteItem}
              index={index}
              editUrl={("/admin/users/", object._id)}
              id={object._id}
            />
          );
          return object;
        });
        setAllUsers(allvalues);
        setIsLoading(false);
      })
      .catch((error) => {});
  }, [is_loading]);

  const deleteItem = (id) => {
    http
      .deleteItem("users/" + id, true)
      .then((response) => {
        if (response.data.status === 200) {
          success(response.data.msg);
          setIsLoading(true);
        } else {
          error(response.data.msg);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <h4>
        All Users
        <NavLink
          to="create"
          className={() => "float-end btn btn-sm btn-success"}
        >
          <FaPlus /> Add User
        </NavLink>
      </h4>
      <hr />
      <DataTable columns={columns} data={allUsers}></DataTable>
      {/* <table className="table table-sm table-hover table-bordered">
        <thead className="table-dark">
          <tr>
            <th>S.N</th>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Nationality</th>
            <th>Role</th>
            <th>Is Admin</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {allUsers.map((obj, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{obj.name}</td>
              <td>{obj.email}</td>
              <td>{obj.gender}</td>
              <td>{obj.nationality}</td>
              <td>{obj.role}</td>
              <td>{obj.is_admin ? "Yes" : "No"}</td>
              <td>
                <button className="btn btn-sm btn-success">
                  <FaPen></FaPen>
                </button>{" "}
                <button className="btn btn-sm btn-danger">
                  <FaTrash></FaTrash>
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table> */}
    </>
  );
}
