import styled from "styled-components";
export const StyledSidebar = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  background-color: white;
  color: white;
  max-width: 250px;
  height: 100vh;

  & div:last-child {
    position: absolute;
    bottom: 2rem;
    width: 100%;
  }
`;

export const SidebarLink = styled.div`
  display: flex;
  color: #fdfdfd;
  margin-left: 1rem;
  gap: 1rem;
  align-items: center;
  height: 3.7rem;
  transition: all 300ms ease;
  & span {
    font-size: 1.6rem;
    transition: all 300ms ease;
  }

  &:hover {
    margin-left: calc(1rem - 3px);
    cursor: pointer;
  }

  & Navlink {
    text-decoration: none;
  }
`;
