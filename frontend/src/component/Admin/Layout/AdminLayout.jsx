import React from "react";
import { Outlet } from "react-router-dom";
import { AdminHeader } from "../../header/headerComponent";
import Sidebar from "../Sidebar/Sidebar";

export default function AdminLayout() {
  return (
    <div id="wrapper">
      <Sidebar></Sidebar>
      <div id="content-wrapper" className="d-flex flex-column">
        <div id="content">
          <AdminHeader></AdminHeader>
          <div className="container-fluid">
            <Outlet></Outlet>
          </div>
        </div>
      </div>
    </div>
  );
}
