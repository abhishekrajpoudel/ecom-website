import React from "react";
import { useNavigate } from "react-router-dom";
import { HttpClient } from "../../../Utils/httpClients";
import { success } from "../../../Utils/utils";
import CategoryForm from "./CategoryForm";

export default function CategoryCreate() {
  const http = new HttpClient();
  const navigate = useNavigate();

  const add = (data) => {
    http
      .postItem("category", data, {
        "content-type": "application/json",
        headers: {
          authorization: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        if (response.data.data) {
          success(response.data.data);
          navigate("/admin/category");
        }
      })
      .catch((error) => {
        console.log("error ", error);
      });
  };

  return <CategoryForm onHandleSubmit={add} data={null}></CategoryForm>;
}
