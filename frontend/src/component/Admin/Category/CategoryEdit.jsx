import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { HttpClient } from "../../../Utils/httpClients";
import { success } from "../../../Utils/utils";
import CategoryForm from "./CategoryForm";

export default function CategoryEdit() {
  const http = new HttpClient();
  const navigate = useNavigate();
  const [data, setData] = useState({});

  const [is_loading, setIsLoading] = useState(true);

  const param = useParams();

  useEffect(() => {
    http
      .getItemById("category/" + param.id, true)
      .then((response) => {
        if (response.data.status === 200) {
          setData(response.data.data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.error("Error", error);
      });
  }, [is_loading]);

  const edit = (data) => {
    http
      .updateItem("category/" + param.id, data, true)
      .then((response) => {
        if (response.data.status === 200) {
          success(response.data.msg);
          navigate("/admin/category");
        }
      })
      .catch((error) => {
        console.error("Error", error);
      });
  };
  return <CategoryForm onHandleSubmit={edit} data={data}></CategoryForm>;
}
