import React from "react";
import { NavLink } from "react-router-dom";
import { useEffect, useState } from "react";
import { error, success } from "../../../Utils/utils";
import { HttpClient } from "../../../Utils/httpClients";
import { FaPlus } from "react-icons/fa";
import DataTable from "react-data-table-component";
import GetActions from "../Action-Button/actionBtnsComponent";

export default function Category() {
  let [allCats, setAllCats] = useState([]);
  const [is_loading, setIsLoading] = useState(true);
  const http = new HttpClient();
  const columns = [
    {
      name: "Title",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "Parent",
      selector: (row) => row.is_parent,
    },
    {
      name: "Action",
      selector: (row) => row.actions,
    },
  ];

  useEffect(() => {
    http
      .getItem(`category`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        let responseValue = response.data.data;
        if (!responseValue) {
          error("No category found 😥");
        }
        let allvalues = responseValue.map((object, index) => {
          object.is_parent = object.is_parent
            ? "Self"
            : responseValue.map((obj) => {
                if (obj._id === object.parent_id) {
                  return obj.name;
                }
              });
          object.actions = (
            <GetActions
              onDelete={deleteItem}
              index={index}
              editUrl={"/admin/category/" + object._id}
              id={object._id}
            />
          );
          return object;
        });

        setAllCats(allvalues);
        setIsLoading(false);
      })
      .catch(() => {});
  }, [is_loading]);

  const deleteItem = (id) => {
    http
      .deleteItem("category/" + id, true)
      .then((response) => {
        if (response.data.status === 200) {
          success(response.data.msg);
          setIsLoading(true);
        } else {
          error(response.data.msg);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <h4>
        All Categories
        <NavLink
          to="create"
          className={() => "float-end btn btn-sm btn-success"}
        >
          <FaPlus /> Add Category
        </NavLink>
      </h4>
      <hr />
      <DataTable columns={columns} data={allCats} />
      {/* <table className="table table-sm table-hover table-bordered">
        <thead className="table-dark">
          <tr>
            <th>S.N</th>
            <th>Title</th>
            <th>Is Parent</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {allCats.map((obj, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{obj.name}</td>
              <td>{obj.is_parent ? "Yes" : "No"}</td>
              <td>
                <button className="btn btn-sm btn-success">
                  <FaPen></FaPen>
                </button>{" "}
                <button
                  className="btn btn-sm btn-danger"
                  onClick={(event) => {
                    return deleteItem(obj._id);
                  }}
                >
                  <FaTrash />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table> */}
    </>
  );
}
