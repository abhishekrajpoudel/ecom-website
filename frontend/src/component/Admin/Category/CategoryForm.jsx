import React, { useState, useEffect } from "react";
import { HttpClient } from "../../../Utils/httpClients";
import { error } from "../../../Utils/utils";
import { FaPaperPlane } from "react-icons/fa";

export default function CategoryForm({ data, onHandleSubmit }) {
  const [title, setTitle] = useState("");
  const [parentId, setParentId] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [allCats, setAllCats] = useState([]);
  const http = new HttpClient();

  useEffect(() => {
    if (data) {
      setTitle(data.name);
      setParentId(data.parent_id);
    }
    http
      .getItem(`category`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      })
      .then((response) => {
        if (!response.data.data) {
          error("No category found.");
        }

        let parent_cats = response.data.data.filter((o) => o.parent_id == null);
        setAllCats(parent_cats);
        setIsLoading(false);
      })
      .catch(() => {});
  }, [isLoading, data]);

  const handleSubmit = (ev) => {
    ev.preventDefault();
    // TODO: Validation, here

    onHandleSubmit({
      name: title,
      is_parent: parentId ? false : true,
      parent_id: parentId ? parentId : null,
    });
  };

  return (
    <>
      <h4>Category Form</h4>
      <hr />
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <form className="form" onSubmit={handleSubmit}>
              <div className="form-group row">
                <label htmlFor="" className="col-sm-3">
                  Title:{" "}
                </label>
                <div className="col-sm-9">
                  <input
                    type="text"
                    name="title"
                    value={title ? title : ""}
                    onChange={(e) => {
                      const { value } = e.target;
                      setTitle(value);
                    }}
                    required
                    placeholder="Enter Category name"
                    className="form-control form-control-sm"
                  />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="" className="col-sm-3">
                  Child of:{" "}
                </label>
                <div className="col-sm-9">
                  <select
                    name="parent_id"
                    value={parentId ? parentId : ""}
                    id="parent_id"
                    onChange={(e) => {
                      setParentId(e.target.value);
                    }}
                    className="form-control form-control-sm"
                  >
                    <option value="">--Select Parent One--</option>
                    {allCats.map((obj, index) => (
                      <option key={index} value={obj._id}>
                        {obj.name}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div id="childCats"></div>
              <div className="form-group row">
                <div className="offset-sm-3 col-sm-9">
                  <button className="btn btn-success btn-sm" type="submit">
                    <FaPaperPlane></FaPaperPlane>&nbsp;
                    {data ? "Update" : "Add"} Category
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
