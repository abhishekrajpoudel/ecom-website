import Dashboard from "./Dashboard/DashboardComponent";
import AdminLayout from "./Layout/AdminLayout";

import Category from "./Category/CategoryComponent";
import CategoryCreate from "./Category/category-create.component";
import CategoryEdit from "./Category/CategoryEdit";

import Users from "./User/UserComponent";
import UserCreate from "./User/UserCreate";
import UserEdit from "./User/UserEdit";

import Products from "./Product/ProductComponent";
import ProductEdit from "./Product/ProductEdit";
import ProductCreate from "./Product/ProductEdit";
export {
  Category,
  CategoryCreate,
  CategoryEdit,
  Users,
  UserCreate,
  UserEdit,
  Dashboard,
  AdminLayout,
  Products,
  ProductCreate,
  ProductEdit,
};
