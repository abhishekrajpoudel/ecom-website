import React from "react";
import { FaPen, FaTrash } from "react-icons/fa";
import { NavLink } from "react-router-dom";

export default function GetActions({ editUrl, onDelete, index, id }) {
  return (
    <>
      <NavLink to={editUrl} className="btn btn-sm btn-success ">
        <FaPen></FaPen>
      </NavLink>{" "}
      <button
        className="btn btn-sm btn-danger"
        onClick={(event) => {
          return onDelete(id);
        }}
      >
        <FaTrash />
      </button>
    </>
  );
}
