import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { Header } from "../header/headerComponent";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";

// const BASE_URL = process.env.REACT_APP_BASE_URL;
const BASE_URL = "http://localhost:9001/api";
export function Login() {
  const [email, setEmail] = useState();
  const [emailErr, setEmailErr] = useState();
  const [password, setPassword] = useState();
  const [passwordErr, setPasswordErr] = useState();
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "email") {
      if (!(value && value.includes("@") && value.includes(".com"))) {
        setEmailErr("Invalid Email format");
      } else {
        setEmailErr("");
      }

      setEmail(value);
    }
    if (name === "password") {
      if (!(value && value.length >= 8)) {
        setPasswordErr("Password length does not match.");
      } else {
        setPasswordErr("");
      }

      setPassword(value);
    }
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();
    console.log("Form Submitted");
    axios
      .post(
        `${BASE_URL}/auth`,
        {
          email: email,
          password: password,
        },
        { header: { "context-type": "application/json" } }
      )
      .then((response) => {
        // localStorage.setItem("isLoggedIn", true);
        if (!response.data.data) {
          toast.error("User Not Found 😭");
        }
        toast.success("Your Now Logged In 👌");
        localStorage.setItem("token", response.data.data.token);
        navigate("/admin");
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    const loggedIn = localStorage.getItem("token");
    if (loggedIn) {
      navigate("/admin");
    }
  });

  return (
    <>
      <Header></Header>
      <div className="container">
        <h4 className="text-center">Login</h4>
        <hr />
        <form action="" method="post" onSubmit={handleSubmit}>
          <div className="mb-3">
            <label className="form-label text-center">Email address</label>
            <input
              name="email"
              type="email"
              className="form-control"
              placeholder="Enter your email"
              onChange={handleChange}
              required
            />
            <span className="text-danger">{emailErr}</span>
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              name="password"
              type="password"
              className="form-control"
              placeholder="Enter your password"
              onChange={handleChange}
              required
            />
            <span className="text-danger">{passwordErr}</span>
          </div>

          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    </>
  );
}

export function Dashboard() {
  const isLoggedIn = localStorage.getItem("token");
  const navigate = useNavigate();
  useEffect(() => {
    if (!isLoggedIn) {
      navigate("/login");
    }
  });
  return <div className="dashboard">Welcome to Dashboard!!</div>;
}

export function Logout() {
  const navigate = useNavigate();
  localStorage.clear();
  useEffect(() => {
    navigate("/login");
  });

  return null;
}
