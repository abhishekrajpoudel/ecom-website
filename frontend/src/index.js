import React from "react";
import ReactDOM from "react-dom";
import App from "./component/app.component";
import "bootstrap/dist/css/bootstrap.min.css";
import "./css/sb-admin-2.css";
ReactDOM.render(<App />, document.getElementById("app"));
